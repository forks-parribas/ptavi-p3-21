#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):
    def __init__(self):
        self.list = []
        self.atributo1 = ""
        self.atributo2 = ""
        self.atributo3 = ""
        self.atributo4 = ""
        self.atributo5 = ""

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'root-layout':
            # De esta manera tomamos los valores de los atributos
            self.atributo1 = attrs.get('width', "")
            self.atributo2 = attrs.get("height", "")
            self.atributo3 = attrs.get("background-color", "")
            self.list.append({'elemento': name,
                              "width": self.atributo1,
                              "height": self.atributo2,
                              "background-color": self.atributo3})

        elif name == 'region':
            self.atributo1 = attrs.get("id", "")
            self.atributo2 = attrs.get("top", "")
            self.atributo3 = attrs.get("bottom", "")
            self.atributo4 = attrs.get("left", "")
            self.atributo5 = attrs.get("right", "")
            self.list.append({'elemento': name,
                              "id": self.atributo1,
                              "top": self.atributo2,
                              "bottom": self.atributo3,
                              "left": self.atributo4,
                              "right": self.atributo5})

        elif name == 'img':
            self.atributo1 = attrs.get("src", "")
            self.atributo2 = attrs.get("region", "")
            self.atributo3 = attrs.get("begin", "")
            self.atributo4 = attrs.get("dur", "")
            self.list.append({'elemento': name,
                              "src": self.atributo1,
                              "region": self.atributo2,
                              "begin": self.atributo3,
                              "dur": self.atributo4})

        elif name == 'audio':
            self.atributo1 = attrs.get("src", "")
            self.atributo2 = attrs.get("begin", "")
            self.atributo3 = attrs.get("region", "")
            self.list.append(
                {'elemento': name, "src": self.atributo1, "begin": self.atributo2, "region": self.atributo3})

        elif name == 'textstream':
            self.atributo1 = attrs.get("src", "")
            self.atributo2 = attrs.get("region", "")
            self.list.append({'elemento': name,
                              "src": self.atributo1,
                              "region": self.atributo2})

    def get_tags(self):
        return self.list


def main():
    """
          Programa principal
       """
    parser = make_parser()
    sHandler = SMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open('karaoke.smil'))
    tags = sHandler.get_tags()
    print(tags)


if __name__ == "__main__":
    main()
