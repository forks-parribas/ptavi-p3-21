#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import sys
from smil import SMILHandler



def main():
    """
          Programa principal
       """
    parser = make_parser()
    sHandler = SMILHandler()
    parser.setContentHandler(sHandler)
    tags = sHandler.get_tags()
    fichero_smil = sys.argv[1]

    parser.parse(open(fichero_smil))

    for atributos in tags: # esto tengo que pasarlo a to_string
        for tags in atributos:
            if atributos[tags] != '':
                print(tags, '=', atributos[tags], end='\t')
        print(end='...')
        print('\n')


if __name__ == "__main__":

    try:
        fichero_smil = sys.argv[1]


    except IndexError:
        sys.exit(
            'Usage: python3 karaoke.py <file>')

    main()


